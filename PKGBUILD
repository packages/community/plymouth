# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Balló György <ballogyor+arch at gmail dot com>
# Contributor: Taijian <taijian@posteo.de>
# Contributor: Sebastian Lau <lauseb644@gmail.com>
# Contributor: Damian01w <damian01w@gmail.com>
# Contributor: Padfoot <padfoot@exemail.com.au>

pkgname=plymouth
pkgver=24.004.60
pkgrel=12
pkgdesc="Graphical boot splash screen"
arch=('x86_64' 'aarch64')
url="https://www.freedesktop.org/wiki/Software/Plymouth/"
license=('GPL-2.0-or-later')
depends=(
  'bash'
  'cairo'
  'cantarell-fonts'
  'filesystem>=2023.12.21'
  'fontconfig'
  'freetype2'
  'glib2'
  'glibc'
  'libdrm'
  'libevdev'
  'libpng'
  'libx11'
  'libxkbcommon'
  'pango'
  'systemd-libs'
  'xkeyboard-config'
)
makedepends=(
  'docbook-xsl'
  'git'
  'gtk3'
  'meson'
)
optdepends=(
  'gtk3: x11 renderer'
)
backup=("etc/$pkgname/${pkgname}d.conf")
install="$pkgname.install"
source=("git+https://gitlab.freedesktop.org/plymouth/$pkgname.git#tag=$pkgver"
        "0001-Revert-src-Hide-console-text-when-splash-is-requeste.patch"
        "${pkgname}d.conf.patch"
        "$pkgname.initcpio_hook"
        "$pkgname.initcpio_install"
        "$pkgname-shutdown.initcpio_install"
        "mkinitcpio-generate-shutdown-ramfs-$pkgname.conf"
        "lxdm-$pkgname.service"
        "lightdm-$pkgname.service"
        "sddm-$pkgname.service"
        "$pkgname-deactivate.service")  # needed for sddm
sha256sums=('7073c84c5cd27336abd0732d4e8b49373fd19ba3fffd8b49ce936208e924e9d5'
            'a1a4c4174fedc7805e7e84ef48bc8f09f7cddad6d6a6d03b97fba3f86cce2a4f'
            '71d34351b4313da01e1ceeb082d9776599974ce143c87e93f0a465f342a74fd2'
            'de852646e615e06d4125eb2e646d0528d1e349bd9e9877c08c5d32c43d288b6f'
            'a8a6e8dc0058253ce6e8f1e455466655757b3f9de1eb0c88f41de32e600296f1'
            '5b518e8cb90b230be5d5151ff77171340e5148c687b8b0833b7f9716a52637d5'
            '91df753b37fa33493a3fa4b966f5535e9694022a4ba083dcc504404ab25715b1'
            '06b31999cf60f49e536c7a12bc1c4f75f2671feb848bf5ccb91a963147e2680d'
            '86d0230d9393c9d83eb7bb430e6b0fb5e3f32e78fcd30f3ecd4e6f3c30b18f71'
            '6aef0053b44db495cb5d55487438758281a626feaaa9166edbf21e2e3a5907f0'
            '3b17ed58b59a4b60d904c60bba52bae7ad685aa8273f6ceaae08a15870c0a9eb')

prepare() {
  cd "$pkgname"

  # Various fixes from upstream
  git cherry-pick -n -m 1 24.004.60..a0e8b6cf50114482e8b5d17ac2e99ff0f274d4c5

  # Use mkinitcpio to update initrd
  sed -i 's/^dracut -f$/mkinitcpio -P/' scripts/plymouth-update-initrd

  patch -Np1 -i ../"${pkgname}d.conf.patch"
}

build() {
  arch-meson build "$pkgname" \
   -Dlogo=/usr/share/pixmaps/manjaro-logo.png
  meson compile -C build
}

package() {
  meson install -C build --destdir "$pkgdir"
  rm -r "$pkgdir/run"

  # Install mkinitcpio hook
  install -Dm644 "$pkgname.initcpio_hook" "$pkgdir/usr/lib/initcpio/hooks/$pkgname"
  install -Dm644 "$pkgname.initcpio_install" "$pkgdir/usr/lib/initcpio/install/$pkgname"

  # Install mkinitcpio shutdown hook and systemd drop-in snippet
  install -Dm644 "$pkgname-shutdown.initcpio_install" \
    "$pkgdir/usr/lib/initcpio/install/$pkgname-shutdown"
  install -Dm644 "mkinitcpio-generate-shutdown-ramfs-$pkgname.conf" \
    "$pkgdir/usr/lib/systemd/system/mkinitcpio-generate-shutdown-ramfs.service.d/$pkgname.conf"

  # Symlink logo for the spinner theme
  ln -s /usr/share/pixmaps/manjaro-logo-text-dark.png \
    "$pkgdir/usr/share/$pkgname/themes/spinner/watermark.png"

  # Symlink depreciated hooks to get a proper warning by mkinitcpio 
  # to notify the user until he fixes it
  ln -s /usr/lib/initcpio/hooks/encrypt "$pkgdir/usr/lib/initcpio/hooks/$pkgname-encrypt"
  ln -s /usr/lib/initcpio/install/encrypt "$pkgdir/usr/lib/initcpio/install/$pkgname-encrypt"
  ln -s "/usr/lib/initcpio/install/$pkgname" "$pkgdir/usr/lib/initcpio/install/sd-$pkgname"

  # Install DM service files
  # Can be replaced by enabling 'lxdm.service', 'lightdm.service' or 'sddm.service' instead
  for i in {sddm,lxdm,lightdm}-plymouth.service; do
    install -Dm644 "${i}" -t "$pkgdir/usr/lib/systemd/system/${i}/"
  done

   # Needed for SDDM
  install -Dm644 "$pkgname-deactivate.service" -t "$pkgdir/usr/lib/systemd/system/"

  # Install default config file
  install -Dm644 "$pkgdir/usr/share/$pkgname/${pkgname}d.defaults" \
    "$pkgdir/etc/$pkgname/${pkgname}d.conf"
}
